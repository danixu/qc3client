/**
 *  @file
 *  @brief A simple Arduino library to set the voltage on a Quick Charge 2.0/3.0 charger.
 */
#pragma once

#include "Arduino.h"

/* 
 *  QC specification constants
 */
 
#define QC3_MIN_VOLTAGE_MV              3600
#define QC3_MAX_VOLTAGE_MV              20000

// timing values for Portable Device are not available, indicative values for a HVDCP charger were taken from several sources
#define QC_T_GLITCH_BC_DONE_MS          1500
#define QC_T_GLICH_V_CHANGE_MS          200
#define QC_T_ACTIVE_MS                  1
#define QC_T_INACTIVE_MS                1

/**
 *  @brief Main class of the QC3Client-library
 *  
 *  The QC3Client-class includes all the functions to easily set the voltage of a Quick Charge 2.0/3.0 source.
 *  
 *  @see setMilliVoltage(unsigned int) 
 */
class QC3Client{
  public:
    /**
     *  @brief Makes an object to control a Quick Charge source.
     *  
     *  @details Makes it possible to set the voltage of the QC source to 5V, 9V, 12V or 20V (with a QC2.0 or later charger) 
     *  or to any supported voltage between 3.6V and 12V (with a QC3.0 or later charger).
     *  Recommended bridge resistors are 47k-6.8k for 5v and 22k-5k for 3.3v. I reccomend to use the 3.3v version using an LDO 
     *  regulator, to allow the charger to lower to 3.6v without break the control circuit
     *  
     *  @param [in] DpPin Data+ pin connected to the middle of the D+ bridge via a 470R resistor
     *  @param [in] DmPin Data- pin connected to the middle of the D- bridge via a 470R resistor
     */
    QC3Control(byte DpPin, byte DmPin);

    /**
     *  @brief Makes an object to control a Quick Charge source.
     *  
     *  @details Makes it possible to set the voltage of the QC source to 5V, 9V, 12V or 20V (with a QC2.0 or later charger) 
     *  or to any supported voltage between 3.6V and 20V (12v of not supported) (with a QC3.0 or later charger).
     *  Recommended bridge resistors are 47k-6.8k for 5v and 22k-5k for 3.3v. I reccomend to use the 3.3v version using an LDO 
     *  regulator, to allow the charger to lower to 3.6v without break the control circuit
     *  
     *  @param [in] DpPin Data+ pin connected to the middle of the D+ bridge via a 470R resistor
     *  @param [in] DmPin Data- pin connected to the middle of the D- bridge via a 470R resistor
     *  @param [in] VoPin V+ of the charger through a 47k-6.8k voltage divider, to control the output voltage.
     */
    QC3Control(byte DpPin, byte DmPin, byte VoPin);

    /**
     *  @brief Starts the handshake with the QC source in "class A" (up to 12V).
     *  
     *  @details A handshake is needed to be able to set the voltage. 
     *  begin() may be left out, in which case the first method modifying the voltage will silently call begin().
     *
     *  begin() is **blocking code**. It waits for a fixed period counting from the start up of the Arduino to act because the handshake needs a minimum time. But this is most likely not a problem because if you need 9V or 12V in your application, there is no gain in proceeding when the voltage isn't there yet (because of the handshake). And by putting begin() (or a call to one of the setXXX() functions) at the end of setup() (or other initialization) you can even do stuff while waiting because it counts from Arduino startup.
     *  
     *  @see begin(bool), setMilliVoltage(unsigned int), set5V(), set9V(), set12V()
     */
    void begin();

    /**
     *  @brief Sets the desired voltage of the QC3.0 source.
     *  
     *  @details Will always set the passed voltage using continuous (QC3) mode (even for 5V, 9V, 12V and 20V). 
     *  To force usage of discrete (QC2) mode, please use set5V(), set9V(), set12V() or set20V();
     *
     *  @note If no handshake has been done (via begin()) with the QC source, the first call to setMilliVoltage() will result in a call to begin() to do the handshake.
     *  @note Setting an unreachable voltage will result in the closest below supported voltage being set.
     *  @note Calling this method on a QC2 charger will not work.
     *  
     *  @see set5V(), set9V(), set12V()
     *
     *  @param [in] milliVolt The desired voltage in mV (between 3600mV and 20000mV).
     *  
     */
    void setMilliVoltage(unsigned int milliVolt);
    
    /**
     *  @brief Return the voltage that the charger is providing.
     *  
     *  @details If feedback ping is used, then returns the real voltage. If not, it just return the voltage that the charger is supposed to be providing.
     *  
     *  @return The voltage that the charger is supposed to currently provide, in milliVolt
     */
    unsigned int getMilliVoltage();
    
    
    /**
     *  @brief Set voltage to 5V
     *  
     *  @details Sets the output of the QC source to 5V using discrete (QC2) mode.
     *  
     *  @note If no handshake has been done (via begin()) with the QC source, the first call to set5V() will result in a call to begin() to do the handshake.
     */
    void set5V();


    /**
     *  @brief Set voltage to 9V
     *  
     *  @details Sets the output of the QC source to 9V using discrete (QC2) mode.
     *  
     *  @note If no handshake has been done (via begin()) with the QC source, the first call to set9V() will result in a call to begin() to do the handshake.
     */
    void set9V();


    /**
     *  @brief Set voltage to 12V
     *  
     *  @details Sets the output of the QC source to 12V using discrete (QC2) mode.
     *  
     *  @note If no handshake has been done (via begin()) with the QC source, the first call to set12V() will result in a call to begin() to do the handshake.
     */
    void set12V();


    /**
     *  @brief Set voltage to 20V
     *  
     *  @details Sets the output of the QC Class B source to 20V using discrete mode.
     *  
     *  @note If no handshake has been done (via begin()) with the QC source, the first call to set20V() will result in a call to begin() to do the handshake.
     */
    void set20V();


    /**
     *  @brief Increment the desired voltage of the QC3.0 source by 200mV.
     *  
     *  @details Will request an increment of the voltage by 200mV. Performing the increment request when the maximum value is already reached has no effect.
     *  
     *  @note If no handshake has been done (via begin()) with the QC source, the first call to incrementVoltage() will result in a call to begin() to do the handshake, then the voltage will be incremented starting from 5V
     */
    void incrementVoltage();


    /**
     *  @brief Decrement the desired voltage of the QC3.0 source by 200mV.
     *  
     *  @details Will request a decrement of the voltage by 200mV. Performing the decrement request when the minimum value is already reached has no effect.
     *  
     *  @note If no handshake has been done (via begin()) with the QC source, the first call to decrementVoltage() will result in a call to begin() to do the handshake, then the voltage will be decremented starting from 5V
     */
    void decrementVoltage();

  
  protected:
    const byte _DpPin; //!< Data+ pin connected to the middle of the D+ 1K5-10K bridge via a 470R resistor 
    const byte _DmPin; //!< Data- pin connected either to the middle of the D- 1K5-10K bridge via a 470R resistor (in recommended "2-wire" circuit), or to the top of the D- 1K5-10K bridge (in legacy "3-wire" circuit)
    const byte _VoPin; //!< FB pin connected to the Vcc of the USB using a 47k-6.8k voltage divider
    
    bool _handshakeDone; //!< Is the handshake done?
    bool _continuousMode; //!< Are we in continuous adjustment (QC3) mode?
  
    unsigned int _milliVoltNow; //!< Voltage currently set (in mV). Using the word "now" instead of "current" to prevent confusion between "current" and "voltage" :-)
    
    void switchToContinuousMode();

    // Low level functions to obtain desired voltages    
    void dmHiZ();
    void dm0V();
    void dm600mV();
    void dm3300mV();
    void dp600mV();
    void dp3300mV();
};

